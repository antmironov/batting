# Instructions

First install dependencies with Bundler:

```
bundle install
```

Run the program with:
```
ruby solution.rb
```

# Filtering

You can pass `--year` or `--team` filters to filter the output by a Team or a Year.

```
ruby solution.rb --team=WS3 --year=1872
```
