require 'csv'
require 'optparse'
require 'terminal-table'

options = {
  input: 'batting.csv'
}

# Parse filters
OptionParser.new do |opts|
  opts.banner = 'Usage: solution.rb [options]'

  opts.on('-iINPUT', '--input=INPUT', String, 'Batting data in CSV format') do |i|
    options[:input] = i
  end

  opts.on('-yYEAR', '--year=YEAR', String, 'Filter by a year') do |y|
    options[:year] = y
  end

  opts.on('-tTEAM', '--team=TEAM', String, 'Filter by a team') do |t|
    options[:team] = t
  end

  opts.on('-h', '--help', 'Prints this help') do
    puts opts
    exit
  end
end.parse!

# Parse teams
teams = {}
CSV.foreach('teams.csv', headers: true) do |team|
  teams[team['teamID']] = team['name']
end

# Parse batting info
data = {}
CSV.foreach(options[:input], headers: true) do |bat|
  next if options[:year] && options[:year] != bat['yearID']
  next if options[:team] && options[:team] != bat['teamID']

  batting = data[[bat['playerID'], bat['yearID']]] || { 'h' => 0.0, 'ab' => 0.0, 'avg' => 0.0, 'teams' => [] }
  batting['teams'] << teams[bat['teamID']]
  batting['h'] += bat['H'].to_f
  batting['ab'] += bat['AB'].to_f
  batting['avg'] = batting['h'].zero? ? 0 : batting['h'] / batting['ab']
  batting['teams'].uniq!
  data[[bat['playerID'], bat['yearID']]] = batting
end

# Print output
data = data.sort_by { |_key, value| value['avg'] }
table = Terminal::Table.new(headings: ['playerID', 'yearId', 'Team name(s)', 'Batting Average']) do |t|
  data.each do |key, value|
    t << [key[0], key[1], value['teams'].join(','), value['avg'].round(3)]
  end
end

puts table
